# Fixed: change setStyle to setFallbackStyle

## Theming problems on Fedora Gnome 31 for qml applications

## org.kde.desktop and ApplicationWindow

- ✅️ control style changes
- ✅ colors change according to the style (adwaita, windows etc)
- ❌ dark theme messed up
- ✅❌ tweaks theme is applied, after app restart

### [Video](/videos/org.kde.desktop-and-ApplicationWindow.mp4)

==========================================

## org.kde.desktop and Kirigami.ApplicationWindow

- ✅ control style changes
- ❌ colors do not change according to the style
- ❌ dark theme doesnt work
- ❌ tweaks theme is NOT applied

### [Video](/videos/org.kde.desktop-and-Kirigami.ApplicationWindow.mp4)

==========================================

## org.kde.desktop.plasma and ApplicationWindow
- ❌ control style does not changes
- ✅ colors change according to the style
- ✅ dark theme works
- ✅❌ tweaks theme is applied, after app restart

### [Video](/videos/org.kde.desktop.plasma-and-ApplicationWindow.mp4)

==========================================

## org.kde.desktop.plasma and Kirigami.ApplicationWindow

- ❌ control style does not changes
- ✅ colors change according to the style
- ✅ dark theme works
- ✅ tweaks theme is applied
- ❌ some controls when disabled look like they are enabled

### [Video](/videos/org.kde.desktop.plasma-and-Kirigami.ApplicationWindow.mp4)
