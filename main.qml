import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.11 as Kirigami

Kirigami.ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    ToolBar {
        id: toolbar
        x: 0; y: 0
        height: 50
        width: parent.width

        ToolButton {
            id: toolButton
            text: state.enabled ? "Disable" : "Enable"
            icon.name: "configure"
            font.pixelSize: Qt.application.font.pixelSize * 1.6

            onClicked: state.enabled = !state.enabled
        }
    }

    ColumnLayout {
        anchors {
            top: toolbar.bottom
            bottom: window.bottom
            left: window.left
            right: window.right
        }

        ToolButton {
            id: btn2
            Layout.alignment: "AlignTop"
            text: "Button"
            icon.name: "configure"
            enabled: state.enabled
            font.pixelSize: Qt.application.font.pixelSize * 1.6

        }

        Item { id: state }

        Label {
            text: "Label"
            enabled: state.enabled
        }

        TextField {
            text: "Label"
            enabled: state.enabled
        }

        Slider {
            from: 0
            to: 1
            value: 0.5
            enabled: state.enabled
        }
    }


    Action {
        shortcut: StandardKey.Quit
        onTriggered: Qt.quit()
    }
}
